# Learning Resources

## Book

Computer System's from a programmer's perspective.

Free book: https://riptutorial.com/Download/intel-x86-assembly-language---microarchitecture.pdf

## Videos

Free videos from the authors:
https://scs.hosted.panopto.com/Panopto/Pages/Sessions/List.aspx#folderID=%22b96d90ae-9871-4fae-91e2-b1627b43e25e%22

Hardware/sofware interface playlist - Luis Ceze & Gaetano Borriello:
https://www.youtube.com/playlist?list=PL0oekSefhQVJdk0hSRu6sZ2teWM740NtL


Good practice in Assembly intel x86:
https://www.codeproject.com/Articles/1116188/40-Basic-Practices-in-Assembly-Language-Programmin#good-reason

Online disa:
https://godbolt.org/

General source of information about assembly:
https://news.ycombinator.com/item?id=16104958

## Other documentation

http://ee.usc.edu/~redekopp/cs356/slides/CS356Unit4_x86_ISA.pdf

https://www.agner.org/optimize/instruction_tables.pdf


# Challenges websites:

## Generic:
https://www.root-me.org/

https://www.hackthebox.eu/

## Reverse engineering:
https://crackmes.one/
